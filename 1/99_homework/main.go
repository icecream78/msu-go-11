package main

import (
	"fmt"
	"sort"
	"strconv"
)

func ReturnInt() int {
	return 1
}

func ReturnFloat() float32 {
	return 1.1
}

func ReturnIntArray() [3]int {
	return [3]int{1, 3, 4}
}

func ReturnIntSlice() []int {
	return []int{1, 2, 3}
}

func IntSliceToString(slice []int) (res string) {
	for _, value := range slice {
		res += strconv.Itoa(value)
	}
	return
}

func MergeSlices(floatSlice []float32, intSlice []int32) (merged []int) {
	for _, value := range floatSlice {
		merged = append(merged, int(value))
	}
	for _, value := range intSlice {
		merged = append(merged, int(value))
	}
	return
}

func GetMapValuesSortedByKey(unorderedMap map[int]string) (orderedList []string) {
	keys := []int{}
	for key := range unorderedMap {
		keys = append(keys, key)
	}

	sort.Ints(keys)

	for _, key := range keys {
		orderedList = append(orderedList, unorderedMap[key])
	}
	return
}

func main() {
	res := IntSliceToString([]int{17, 23, 100500})
	fmt.Println(res)
}
